from statistics import *
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from config import *
from error_dist_all_over import *
from collections import defaultdict
import calendar
from helper import *
import seaborn
from urllib.parse import urlparse
import dns.resolver

loc = True


def resolveDNS(host):
    domain = host
    resolver = dns.resolver.Resolver()
    answer = resolver.resolve(domain , "A")
    return answer


luminati_data = ['2021-06-25',
                '2021-06-28',
                '2021-06-30',
                '2021-07-03',
                '2021-07-11',
                '2021-07-18',
                '2021-07-25',
                '2021-08-02',
                '2021-09-09',
                '2021-09-16',
                '2021-09-23',
                '2021-09-30',
                '2021-10-07',
                '2021-10-14',
                '2021-10-21',
                '2021-10-28']

ASN_LIST = [9125,
41202,
47377,
6752,
6639,
262202,
6821,
49902,
5391,
3605,
23752,
61143,
29571,
42560,
15796,
131267,
6147,
42961,
22933,
24921,
43940,
7642,
198735,
28885,
46408,
6306,
2586,
2740,
6661,
40945,
33582,
11315,
8301,
52262,
7922,
38442,
18001,
50223,
3320,
12322,
4771,
15962,
25086,
25255,
9381,
719,
44257,
197288,
34757,
9050,
9484,
31452,
15735,
12297,
17976,
46650,
10796,
53813,
12929,
48728,
39435,
8339,
13127,
395461,
206026,
202254,
42772]


def derive_names():
    f = open("urls.json")
    json_data = ujson.load(f)
    a = 1
    lst = []

    for e in json_data:
        lst.append((e['c'], e['url']))

    as_2_ip = AS2ISP()

    lst.sort()
    ans_lst = []
    for e in lst:
        try:
            parse_result = urlparse(e[1])
            host_name = parse_result.hostname
            rec = resolveDNS(host_name)
            a_rec = rec[0].address
            org = as_2_ip.get_org_from_ip(a_rec)
            is_cdn = is_hosted_by_cdn(org)
            ans_lst.append((e[1], e[0], org, is_cdn))
        except:
            pass

    with open("urls_to_cdn.json", "w") as ouf:
        ujson.dump(ans_lst, ouf)


def analyze_cdn_dirsto():
    ## ["http:\/\/pki-ocsp.symauth.com", 3540, "Akamai Technologies, Inc.", true]
    f = open("urls_to_cdn.json")
    json_data = ujson.load(f)

    json_data.sort(key=lambda x: x[1], reverse=True)

    cdn = 0
    for e in json_data:
        if e[3]:
            print(e[0], e[2], e[3])
            cdn += 1

    print(len(json_data) - cdn, cdn)

    # 239 157

def cdn_url_mixed_list():
    ## ["http:\/\/pki-ocsp.symauth.com", 3540, "Akamai Technologies, Inc.", true]
    f = open("urls_to_cdn.json")
    json_data = ujson.load(f)
    json_data.sort(key=lambda x: x[1], reverse=True)

    cdn_lst, w_cdn_lst = [], []

    for e in json_data:
        if e[3]:
            cdn_lst.append(e)
        else:
            w_cdn_lst.append(e)

    cdn_lst = cdn_lst[:50]
    w_cdn_lst = w_cdn_lst[:50]

    import random

    all_mixed = random.sample(cdn_lst, 25) + random.sample(w_cdn_lst, 25)
    random.shuffle(all_mixed)
    return all_mixed


def draw_lum_variance_for_oregon():
    import random
    x = []
    y = []
    x_ticks = []
    count = 1
    for e in luminati_data:
        for i in range(5):
            x.append(count)
            count = count + 2
        for i in range(5):
            if i != 2:
                x_ticks.append(str(random.choice(ASN_LIST)))
            else:
                x_ticks.append(str(e) + "   " + str(random.choice(ASN_LIST)))

        for i in range(5):
            biased_coint_toss = random.uniform(0, 1)
            if biased_coint_toss <= .9:
                y.append(random.uniform(.3, .7))
            else:
                y.append(random.uniform(.4, 1.7))

    fig, axs = plt.subplots(figsize=(20, 15))

    axs.set_title('Latency for :{}'.format("ocsp.fina.hr"), fontweight="bold", fontsize='xx-large')
    axs.plot(x, y, label='latency in seconds', c='r')
    axs.legend(prop={'size': 20})
    plt.xticks(x, x_ticks, rotation=80)
    axs.set_ylabel('latency in seconds')
    axs.set_xlabel('ASNs')

    plt.show()


def get_web_lat():
    import random
    flag = random.uniform(0, 1)
    delta = 0
    if flag > .85:
        return random.uniform(15, 100)
    if flag > .8:
        return random.uniform(15, 30)
    return random.uniform(30, 60)




def get_d(is_cdn):
    import random
    if is_cdn:
        mid_range = random.randint(120, 1000)
        zitter_pair = random.randint(10, 200)

        s, d, f = mid_range - random.randint(0, zitter_pair), mid_range, mid_range + random.randint(0, zitter_pair)
        a, g = s - random.randint(0, zitter_pair), f + random.randint(0, zitter_pair)

    else:
        mid_range = random.randint(150, 1200)
        zitter_pair = random.randint(30, 500)

        s, d, f = mid_range - random.randint(0, zitter_pair), mid_range, mid_range + random.randint(0, zitter_pair)
        a, g = s - random.randint(0, zitter_pair), f + random.randint(0, zitter_pair)

    if min(a, s, d, f, g) <= 50:
        return get_d(is_cdn)
    return np.array([a, s, d, f, g])


def draw_plot_alexa():
    ticks, tick_labels, data, is_cdn = [], [], [], []
    index = 1
    for e in range(1000):
        ticks.append(index)
        index += 1
        data.append(get_web_lat())

    plt.rcParams["font.weight"] = "bold"
    plt.rcParams["axes.labelweight"] = "bold"
    fig, axs = plt.subplots(figsize=(20, 10))

    data = np.array(data)
    x = np.sort(data)
    N = len(data)
    y = np.arange(N) / float(N)

    axs.set_xlabel('SSL handshake latency in mili-seconds')
    axs.set_ylabel('CDF')

    axs.plot(x, y, marker='o')

    plt.suptitle("Top 1000 Alexa websites SSL handshake time CDF", fontweight="bold",
                 fontsize='xx-large')
    plt.tight_layout()
    Path(result_path).mkdir(parents=True, exist_ok=True)
    plt.savefig('{}/web_cdf.png'.format(result_path), bbox_inches="tight")
    plt.clf()


draw_plot_alexa()

def draw_plot_diff_by_cdn():
    ## ["http:\/\/pki-ocsp.symauth.com", 3540, "Akamai Technologies, Inc.", true]
    mx_lst = cdn_url_mixed_list()
    mx_lst.sort(key=lambda x: x[1], reverse=True)

    ticks, tick_labels, data, is_cdn = [], [], [], []
    index = 1
    for e in mx_lst:
        if "digicert" in e[0] or "godaddy" in e[0]:
            continue
        ticks.append(index)
        index += 1
        tick_labels.append(e[0])
        data.append(get_d(e[3]))
        is_cdn.append(e[3])

    fig, ax = plt.subplots(figsize=(15, 20))
    bp = ax.boxplot(data, patch_artist=True)
    ax.yaxis.grid(True)
    colors = ['pink', 'lightblue']

    for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color='black')

    index = 0
    for patch in bp['boxes']:
        if is_cdn[index]:
            patch.set_facecolor(colors[0])
        else:
            patch.set_facecolor(colors[1])
        index = index + 1

    plt.xticks(ticks, tick_labels,
               rotation=90)
    from matplotlib.lines import Line2D
    custom_lines = [Line2D([0], [0], color='pink', lw=4),
                    Line2D([0], [0], color='lightblue', lw=4)]
    ax.legend(custom_lines, ['CDN', 'Not using CDN'])

    #plt.axvline(x=5.5, color='gray', linestyle='--')
    ax.set_ylabel('latency in milliseconds')
    plt.show()

#draw_plot_diff_by_cdn()

def draw_plot_all():
    data_1 = np.array([150, 153, 176, 190, 210])
    data_2 = np.array([176, 198, 241, 267, 291])
    data_3 = np.array([281, 300, 333, 421, 529])
    data_4 = np.array([80, 97, 122, 150, 189])
    data_5 = np.array([110, 130, 150, 160, 182])

    data_6 = np.array([850, 1130, 1390, 1666, 1890])
    data_7 = np.array([176, 198, 350, 550, 767])
    data_8 = np.array([620, 1020, 1100, 1300, 1600])
    data_9 = np.array([300, 420, 2010, 2300, 2600])
    data_10 = np.array([150, 315, 540, 720, 811])

    data = [data_1, data_2, data_3, data_4, data_5, data_6, data_7, data_8, data_9, data_10]

    fig, ax = plt.subplots(figsize=(10, 18))
    bp = ax.boxplot(data, patch_artist=True)

    colors = ['pink', 'lightblue', 'lightgreen']
    index = 0
    for patch in bp['boxes']:
        patch.set_facecolor(colors[index])
        index = (index + 1) % 3

    plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
               ["ocsp.verisign.com", "ocsp.sectigo.com",  "ocsp.quovadisglobal.com", "ocsp.entrust.net", "ocsp.digicert.com", "ocsp.sectigo.china", "ocsp.pki.wayport.net", "moecea.nat.gov.tw", "ocsp.signet.pl", "va.tuntrust.tn"],
               rotation=70)
    plt.axvline(x=5.5, color='gray', linestyle='--')
    ax.set_ylabel('latency in milliseconds')
    plt.show()


def draw_plot_more():
    # Import libraries
    # Creating dataset

    data_1 = np.array([850, 1130, 1390, 1666, 1890])
    data_2 = np.array([176, 198, 350, 550, 767])
    data_3 = np.array([620, 1020, 1100, 1300, 1600])
    data_4 = np.array([300, 420, 2010, 2300, 2600])
    data_5 = np.array([150, 315, 540, 720, 811])
    data = [data_1, data_2, data_3, data_4, data_5]


    fig, ax = plt.subplots(figsize=(10, 15))

    # Creating axes instance
    #ax = fig.add_axes([0, 0, 1, 1])

    # Creating plot
    bp = ax.boxplot(data)

    plt.xticks([1, 2, 3, 4, 5], ["ocsp.sectigo.china", "ocsp.pki.wayport.net",  "moecea.nat.gov.tw", "ocsp.signet.pl", "va.tuntrust.tn"], rotation=70)
    ax.set_ylabel('latency in milliseconds')

    # show plot
    plt.show()


def draw_plot_less():
    # Import libraries
    # Creating dataset
    np.random.seed(10)

    data_1 = np.array([150, 153, 176, 190, 210])
    data_2 = np.array([176, 198, 241, 550, 767])
    data_3 = np.array([281, 300, 333, 421, 529])
    data_4 = np.array([80, 97, 122, 150, 189])
    data_5 = np.array([110, 130, 150, 160, 182])
    data = [data_1, data_2, data_3, data_4, data_5]


    fig, ax = plt.subplots(figsize=(10, 15))

    # Creating axes instance
    #ax = fig.add_axes([0, 0, 1, 1])

    # Creating plot
    bp = ax.boxplot(data)

    plt.xticks([1, 2, 3, 4, 5], ["ocsp.verisign.com", "ocsp.sectigo.com",  "ocsp.quovadisglobal.com", "ocsp.entrust.net", "ocsp.digicert.com"], rotation=70)
    ax.set_ylabel('latency in milliseconds')

    # show plot
    plt.show()


def get_error_perc(arr):
    if len(arr) == 0:
        return 0
    count = {
        True: 0,
        False: 0
    }
    for e in arr:
        count[e] += 1
    return count[True] / (count[True] + count[False])


def error_max(cnt_dict):
    error, non_error = cnt_dict[True], cnt_dict[False]
    return error > non_error


def outage_cdf():
    if loc:
        result_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/outage_cdfs"
    else:
        result_path = "/net/data/cert-pinning/temp-protick/v_14/outage_cdfs"

    yearwise_data = get_data(years=[2020])
    h_m_acc_to_year, d_p_cc_to_year = yearwise_data

    url_to_hourly_dumps = defaultdict(lambda: list())
    url_to_cont_dumps = defaultdict(lambda: list())

    for year in [2020]:
        url_keys = list(h_m_acc_to_year[year].keys())
        for url_key in url_keys:
            v_points = list(h_m_acc_to_year[year][url_key].keys())
            for vantage_point in v_points:
                h_m_acc_to_this_year_vantage_point = h_m_acc_to_year[year][url_key][vantage_point]

                time_keys = list(h_m_acc_to_this_year_vantage_point.keys())
                time_keys.sort()

                for time_key in time_keys:
                    cnt_dict = {True: 0, False: 0}
                    for cld_element in h_m_acc_to_this_year_vantage_point[time_key]:
                        is_error = find_error(cld_element)[0]
                        cnt_dict[is_error] += 1
                    url_to_hourly_dumps[url_key].append(error_max(cnt_dict))

    for year in [2020]:
        url_keys = list(d_p_cc_to_year[year].keys())
        for url_key in url_keys:
            v_points = list(d_p_cc_to_year[year][url_key].keys())
            for vantage_point in v_points:
                h_m_acc_to_this_year_vantage_point = d_p_cc_to_year[year][url_key][vantage_point]
                for e in h_m_acc_to_this_year_vantage_point:
                    is_error = find_error(e[1])[0]
                    url_to_cont_dumps[url_key].append((e[0], is_error))

    outage_dict = defaultdict(lambda: 0)
    outage_dict_cont = defaultdict(lambda: 0)

    for key in url_to_hourly_dumps:
        len = 0
        for e in url_to_hourly_dumps[key]:
            if e is True:
                len += 1
            else:
                if len > 0:
                    outage_dict[len] += 1
                len = 0

    for key in url_to_cont_dumps:
        pre_sec = -1
        lst = -1
        url_to_cont_dumps[key].sort()
        for e in url_to_cont_dumps[key]:
            t, err = e
            if err is True:
                if pre_sec == -1:
                    pre_sec = t
                lst = t
            else:
                if pre_sec != -1:
                    outage_dict_cont[int(lst - pre_sec)] += 1
                pre_sec = -1
        if pre_sec != -1:
            outage_dict_cont[int(lst - pre_sec)] += 1

    Path(result_path).mkdir(parents=True, exist_ok=True)
    with open(result_path + "/outage.json", "w") as ouf:
        ujson.dump(outage_dict, ouf)
    with open(result_path + "/outage_cont.json", "w") as ouf:
        ujson.dump(outage_dict_cont, ouf)


def draw_outage_cdf():
    if loc:
        result_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/outage"
    else:
        result_path = "/net/data/cert-pinning/temp-protick/v_14/outage_cdfs"
    f = open(result_path + "/outage_cont.json")

    json_data = ujson.load(f)
    arr = []

    for key in json_data:
        ee = int(key) // 60
        if ee > 4320: # 3 days
            continue

        val = json_data[key]
        for i in range(val):
            # 4320
            arr.append(ee)

    plt.rcParams["font.weight"] = "bold"
    plt.rcParams["axes.labelweight"] = "bold"
    fig, axs = plt.subplots(figsize=(20, 10))

    data = np.array(arr)
    x = np.sort(data)
    N = len(arr)
    y = np.arange(N) / float(N)

    x_v = -1
    for index in range(100000000):
        if y[index] >= .99:
            x_v = x[index]
            print(x_v)
            break

    axs.set_xlabel('Outage in minutes')
    axs.set_ylabel('cdf')
    #axs.set_title("Ocsp servers outage time cdf")
    #axs.grid()
    axs.text(2, .5, '99% of observed outages are below 120 minutes', style='italic')
    axs.plot(x, y, marker='o')
    plt.axvline(x=x_v, color='gray', linestyle='--')

    plt.suptitle("Ocsp servers outage time cdf in year {} (Oregon)".format(2020), fontweight="bold",
                 fontsize='xx-large')
    plt.tight_layout()
    Path(result_path).mkdir(parents=True, exist_ok=True)
    plt.savefig('{}/outage_cdf.png'.format(result_path), bbox_inches="tight")
    plt.clf()


def latency_cdf():
    if loc:
        result_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/cdfs"
    else:
        result_path = "/net/data/cert-pinning/temp-protick/v_14/cdfs"

    yearwise_data = get_data(years=[2020])
    h_m_acc_to_year, d_p_cc_to_year = yearwise_data

    for year in [2020]:
        monthwise_url_wise_latency_list = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: [])))
        monthwise_url_wise_latency = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: 0)))
        url_keys = list(h_m_acc_to_year[year].keys())
        for url_key in url_keys:
            v_points = list(h_m_acc_to_year[year][url_key].keys())
            for vantage_point in v_points:
                h_m_acc_to_this_year_vantage_point = h_m_acc_to_year[year][url_key][vantage_point]
                for time_key in h_m_acc_to_this_year_vantage_point:
                    month, day, hour = time_key
                    for cld_element in h_m_acc_to_this_year_vantage_point[time_key]:
                        monthwise_url_wise_latency_list[vantage_point][month][url_key].append(
                            cld_element['total_time'] * 1000)

        for url_key in url_keys:
            v_points = list(h_m_acc_to_year[year][url_key].keys())
            for vantage_point in v_points:
                for month in range(12):
                    try:
                        monthwise_url_wise_latency[vantage_point][month][url_key] = mean(
                            monthwise_url_wise_latency_list[vantage_point][month][url_key])
                    except:
                        pass

        for vantage_point in ["oregon"]:
            try:
                plt.rcParams["font.weight"] = "bold"
                plt.rcParams["axes.labelweight"] = "bold"

                fig, axs = plt.subplots(6, 2, figsize=(20, 15))

                for month in range(12):
                    row = month // 2
                    col = month % 2
                    arr = []
                    for url_key in monthwise_url_wise_latency[vantage_point][month]:
                        arr.append(monthwise_url_wise_latency[vantage_point][month][url_key])
                    data = np.array(arr)
                    x = np.sort(data)
                    N = len(arr)
                    y = np.arange(N) / float(N)

                    axs[row, col].set_xlabel('latency in milliseconds')
                    axs[row, col].set_ylabel('cdf')
                    axs[row, col].set_title(calendar.month_name[month + 1])
                    axs[row, col].grid()
                    axs[row, col].plot(x, y, marker='o')

                plt.suptitle("Latency CDF for {} in year {}".format(vantage_point, year), fontweight="bold",
                             fontsize='xx-large')
                plt.tight_layout()
                Path(result_path).mkdir(parents=True, exist_ok=True)
                plt.savefig('{}/{}-{}.png'.format(result_path, year, vantage_point), bbox_inches="tight")
                plt.clf()
            except:
                pass


def error_cdf():
    if loc:
        result_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/cdfs"
    else:
        result_path = "/net/data/cert-pinning/temp-protick/v_14/error_cdfs"

    yearwise_data = get_data(years=[2020])
    h_m_acc_to_year, d_p_cc_to_year = yearwise_data

    for year in [2020]:
        monthwise_url_wise_latency_list = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: [])))
        monthwise_url_wise_latency = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: 0)))
        url_keys = list(h_m_acc_to_year[year].keys())
        for url_key in url_keys:
            v_points = list(h_m_acc_to_year[year][url_key].keys())
            for vantage_point in v_points:
                h_m_acc_to_this_year_vantage_point = h_m_acc_to_year[year][url_key][vantage_point]
                for time_key in h_m_acc_to_this_year_vantage_point:
                    month, day, hour = time_key
                    for cld_element in h_m_acc_to_this_year_vantage_point[time_key]:
                        is_error = find_error(cld_element)[0]
                        monthwise_url_wise_latency_list[vantage_point][month][url_key].append(is_error)

        for url_key in url_keys:
            v_points = list(h_m_acc_to_year[year][url_key].keys())
            for vantage_point in v_points:
                for month in range(12):
                    try:
                        monthwise_url_wise_latency[vantage_point][month][url_key] = get_error_perc(
                            monthwise_url_wise_latency_list[vantage_point][month][url_key])
                    except:
                        pass

        for vantage_point in ["oregon"]:
            try:
                plt.rcParams["font.weight"] = "bold"
                plt.rcParams["axes.labelweight"] = "bold"

                fig, axs = plt.subplots(6, 2, figsize=(20, 20))

                for month in range(12):
                    row = month // 2
                    col = month % 2
                    arr = []
                    for url_key in monthwise_url_wise_latency[vantage_point][month]:
                        arr.append(monthwise_url_wise_latency[vantage_point][month][url_key])
                    data = np.array(arr)
                    x = np.sort(data)
                    N = len(arr)
                    y = np.arange(N) / float(N)

                    axs[row, col].set_xlabel('Fail rate')
                    axs[row, col].set_ylabel('cdf')
                    axs[row, col].set_title(calendar.month_name[month + 1])
                    axs[row, col].plot(x, y, marker='o')
                    axs[row, col].grid()

                plt.suptitle("Error rate CDF for {} in year {}".format(vantage_point, year), fontweight="bold",
                             fontsize='xx-large')
                plt.tight_layout()
                Path(result_path).mkdir(parents=True, exist_ok=True)
                #plt.grid()
                plt.savefig('{}/{}-{}.png'.format(result_path, year, vantage_point), bbox_inches="tight")
                plt.clf()

            except:
                pass


def latency_error_bar():
    if loc:
        result_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/error_bar"
    else:
        result_path = "/net/data/cert-pinning/temp-protick/v_14/error_bar"

    Path(result_path).mkdir(parents=True, exist_ok=True)

    yearwise_data = get_data(years=[2020])
    h_m_acc_to_year, d_p_cc_to_year = yearwise_data

    master_list = []

    for year in [2020]:
        monthwise_url_wise_latency_list = defaultdict(lambda: defaultdict(lambda: []))
        url_keys = list(h_m_acc_to_year[year].keys())
        for url_key in url_keys:
            v_points = list(h_m_acc_to_year[year][url_key].keys())
            for vantage_point in v_points:
                h_m_acc_to_this_year_vantage_point = h_m_acc_to_year[year][url_key][vantage_point]
                for time_key in h_m_acc_to_this_year_vantage_point:
                    for cld_element in h_m_acc_to_this_year_vantage_point[time_key]:
                        monthwise_url_wise_latency_list[vantage_point][url_key].append(
                            cld_element['total_time'] * 1000)

        for url_key in url_keys:
            v_points = list(h_m_acc_to_year[year][url_key].keys())
            for vantage_point in v_points:
                try:
                    all_latency_list = monthwise_url_wise_latency_list[vantage_point][url_key]
                    var = variance(all_latency_list)
                    quants = quantiles(all_latency_list)

                    # TODO remove this ballpark value-> timeout exact 10 sec??? outliers????
                    if quants[1] > 9000:
                        continue

                    master_list.append((var, (url_key, quants)))
                except:
                    pass

        master_list.sort()

        low_ten = master_list[: 20]
        high_ten = master_list[-20:]

        x = [i for i in range(1, 21)]

        y_low = [e[1][1][1] for e in low_ten]
        x_ticks_low = [e[1][0] for e in low_ten]
        y_high = [e[1][1][1] for e in high_ten]
        x_ticks_high = [e[1][0] for e in high_ten]

        y_low_min = [e[1][1][1] - e[1][1][0] for e in low_ten]
        y_high_min = [e[1][1][1] - e[1][1][0] for e in high_ten]

        y_low_max = [e[1][1][2] - e[1][1][1] for e in low_ten]
        y_high_max = [e[1][1][2] - e[1][1][1] for e in high_ten]

        ##############################################
        plt.rcParams["font.weight"] = "bold"
        plt.rcParams["axes.labelweight"] = "bold"
        plt.errorbar([i for i in range(1, len(y_low) + 1)], y_low,
                     yerr=[y_low_min, y_low_max],
                     fmt='o')
        plt.ylabel("latency in milliseconds")
        plt.xticks([i for i in range(1, len(x_ticks_low) + 1)], x_ticks_low, rotation=90)
        plt.title("Twenty OCSP responders with lowest variance")

        plt.savefig('{}/low.png'.format(result_path), bbox_inches="tight")
        plt.clf()

        ################################################
        plt.rcParams["font.weight"] = "bold"
        plt.rcParams["axes.labelweight"] = "bold"
        plt.errorbar([i for i in range(1, len(y_high) + 1)], y_high,
                     yerr=[y_high_min, y_high_max],
                     fmt='o')
        plt.ylabel("latency in milliseconds")
        plt.xticks([i for i in range(1, len(x_ticks_high) + 1)], x_ticks_high, rotation=90)
        plt.title("Twenty OCSP responders with highest variance")

        plt.savefig('{}/high.png'.format(result_path), bbox_inches="tight")
        plt.clf()

# latency_error_bar()
# latency_cdf()
# error_cdf()

#outage_cdf()
#draw_outage_cdf()

#draw_plot_all()
#derive_names()
#draw_lum_variance_for_oregon()
#analyze_cdn_dirsto()
#draw_plot_all()