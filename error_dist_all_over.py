import datetime
import os
import time
from pathlib import Path
import calendar

import matplotlib.pyplot as plt
import ujson

from config import *
from helper import *
from caida_as_org import *

urls_to_consider = ["ocsp.comodoca.com",
"ocsp.digicert.com",
"ocsp.godaddy.com",
"evocsp.cybertrust.ne",
"apsrsa12g101",
"hcaocps.nat.gov",
"wildca.ocsp.dhimyotis.com",
"ocsp.oaticert.com",
"wildca.ocsp.dhimyotis.com",
"gsatlasr3dvtlsca2020"]

# urls_to_consider = ["ocsp.comodoca.com"]

def is_url_allowed(url, local_env):
    return 'all_urls' not in url



def get_directories_or_files_from_path(path, is_dir = True):
    retset = set()
    #print(path)
    for parent_path in os.listdir(path):
        #print(parent_path)

        if not os.path.isdir(path + "/" + parent_path):
            if is_dir:
                continue
        else:
            if not is_dir:
                continue
        retset.add((parent_path, path + "/" + parent_path))

    return list(retset)

local_env = True
if local_env:
    root_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/ocsp_url_wise_dumps_v2"
    result_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/graphs_combined"
else:
    root_path = "/net/data/cert-pinning/temp-protick/v_14/url_wise"
    result_path = "/net/data/cert-pinning/temp-protick/v_14/graphs_combined_v2"

error_set = set()
from collections import defaultdict
d = defaultdict(lambda: 0)


def get_col(index):
    if index < len(col_ultra):
        return col_ultra[index]
    else:
        import random
        r = random.random()
        b = random.random()
        g = random.random()
        color = (r, g, b)
        return color


def plot_graphs_v2(hour_month_wise_data, year_in_consideration, mode, url_key, as_2_ip):

    plt.rcParams["font.weight"] = "bold"
    plt.rcParams["axes.labelweight"] = "bold"

    full_desc = url_key + "-{}-{}".format(year_in_consideration, mode)
    save_dir = '{}/{}/{}/{}/'.format(result_path, mode, year_in_consideration, proc_url(url_key))

    Path(save_dir).mkdir(parents=True, exist_ok=True)

    fig, axs = plt.subplots(2, 1, figsize=(15, 10))
    x_tick_info = [[], []]

    col_index = -1

    over_lap_dict = defaultdict(lambda: 0)

    for vantage_point in hour_month_wise_data:
        hourly_keys = set(list(hour_month_wise_data[vantage_point].keys()))
        for key in hourly_keys:
            over_lap_dict[key] += 1

    total_vantage_points = len(list(hour_month_wise_data.keys()))

    master_keys = []
    for key in over_lap_dict:
        if over_lap_dict[key] >= total_vantage_points:
            master_keys.append(key)
    master_keys = list(set(master_keys))
    master_keys.sort()

    index, month = 0, 1
    for e in master_keys:
        index += 1
        if e[0] == month:
            x_tick_info[0].append(index)
            x_tick_info[1].append(calendar.month_name[month])
            month += 1

    ip_set = set()
    org_set = set()
    ip_to_org_dict = {}
    #ip_to_col_dict = {}
    #org_to_col_dict = {}

    org_v_to_col_dict = defaultdict(lambda: dict())

    ip_v_set = set()
    org_v_set = set()

    for vantage_point in hour_month_wise_data:
        for key in hour_month_wise_data[vantage_point]:
            for element in hour_month_wise_data[vantage_point][key]:
                ip = element['primary_ip']
                #org = as_2_ip.get_org_from_ip(ip)
                ip_set.add(ip)
                ip_v_set.add((ip, vantage_point))
                #ip_to_org_dict[ip] = org

    for ip in ip_set:
        org = as_2_ip.get_org_from_ip(ip)
        ip_to_org_dict[ip] = org
        org_set.add(org)

    for ip_v in ip_v_set:
        ip, v = ip_v
        org = ip_to_org_dict[ip]
        org_v_set.add((org, v))


    master_color_index = 0
    for org_v in org_v_set:
        org, vantage_p = org_v
        org_v_to_col_dict[org][vantage_p] = get_col(master_color_index)
        #org_to_col_dict[org] = col_ultra[master_color_index]
        master_color_index += 1

    patches = []
    marker_list = [".", "*", "v", "1", "s", "X", "_"]
    marker_index = -1
    for vantage_point in hour_month_wise_data:
        marker_index += 1
        is_org_visited = {}
        import matplotlib.patches as mpatches

        col_index += 1
        hourly_keys = list(hour_month_wise_data[vantage_point].keys())
        hourly_keys.sort()
        hourly_keys = [x for x in hourly_keys if over_lap_dict[x] >= total_vantage_points]

        x, y, y_lat, lat_col = [], [], [], []
        hourly_key_to_count = defaultdict(lambda: (0, 0))
        hourly_key_to_error_count = defaultdict(lambda: defaultdict(lambda: 0))

        hourly_key_to_med_latency = {}
        for key in hourly_keys:
            lat_list = []
            ip_most_list = []
            for element in hour_month_wise_data[vantage_point][key]:
                err, succ = hourly_key_to_count[key]
                if element['error_details'][0]:
                    err += 1
                    hourly_key_to_error_count[key][element['error_details'][1]] += 1
                else:
                    succ += 1
                hourly_key_to_count[key] = (err, succ)
                lat_list.append(element['total_time'])
                ip_most_list.append(element['primary_ip'])
            import statistics
            chosen_ip = max(set(ip_most_list), key=ip_most_list.count)
            org = ip_to_org_dict[chosen_ip]
            if org not in is_org_visited:
                is_org_visited[org] = 1
                patches.append(mpatches.Patch(color=org_v_to_col_dict[org][vantage_point], label="{}-{}".format(vantage_point, org)))

            hourly_key_to_med_latency[key] = (statistics.median(lat_list), org)

        index = 1
        for key in hourly_keys:
            err, succ = hourly_key_to_count[key]
            if (succ + err) == 0:
                ratio = 1
            else:
                ratio = succ / (succ + err)
            x.append(index)
            y.append(ratio)
            y_lat.append(hourly_key_to_med_latency[key][0])
            lat_col.append(org_v_to_col_dict[hourly_key_to_med_latency[key][1]][vantage_point])

            index += 1

        axs[0].plot(x, y, label=vantage_point, c=get_col(index), linewidth=5, alpha=0.5, marker=marker_list[marker_index])
        axs[1].scatter(x, y_lat, label=vantage_point, c=lat_col, s=.3)

    axs[0].set_title('Hourly Successful Response Ration for {}'.format(full_desc), fontweight="bold",
                     fontsize='xx-large')
    axs[0].legend(prop={'size': 20})
    plt.sca(axs[0])
    plt.xticks(x_tick_info[0], x_tick_info[1])
    axs[1].set_title('Hourly Median Latency for :{}'.format(full_desc), fontweight="bold", fontsize='xx-large')
    #axs[1].legend(prop={'size': 20})
    axs[1].legend(handles=patches)
    plt.sca(axs[1])
    plt.suptitle(url_key, fontweight="bold", fontsize='xx-large')
    plt.savefig('{}{}.png'.format(save_dir, "all_graphs.png"), bbox_inches="tight")
    plt.clf()


def plot_graphs(hour_month_wise_data, year_in_consideration, mode, url_key, as_2_ip):

    plt.rcParams["font.weight"] = "bold"
    plt.rcParams["axes.labelweight"] = "bold"

    full_desc = url_key + "-{}-{}".format(year_in_consideration, mode)
    save_dir = '{}/{}/{}/{}/'.format(result_path, mode, year_in_consideration, proc_url(url_key))

    Path(save_dir).mkdir(parents=True, exist_ok=True)

    fig, axs = plt.subplots(2, 1, figsize=(15, 10))
    x_tick_info = [[], []]

    col_index = -1

    over_lap_dict = defaultdict(lambda: 0)

    for vantage_point in hour_month_wise_data:
        hourly_keys = set(list(hour_month_wise_data[vantage_point].keys()))
        for key in hourly_keys:
            over_lap_dict[key] += 1

    total_vantage_points = len(list(hour_month_wise_data.keys()))

    master_keys = []
    for key in over_lap_dict:
        if over_lap_dict[key] >= total_vantage_points:
            master_keys.append(key)
    master_keys = list(set(master_keys))
    master_keys.sort()

    index, month = 0, 1
    for e in master_keys:
        index += 1
        if e[0] == month:
            x_tick_info[0].append(index)
            x_tick_info[1].append(calendar.month_name[month])
            month += 1

    for vantage_point in hour_month_wise_data:
        col_index += 1
        hourly_keys = list(hour_month_wise_data[vantage_point].keys())
        hourly_keys.sort()
        hourly_keys = [x for x in hourly_keys if over_lap_dict[x] >= total_vantage_points]

        x, y, y_lat = [], [], []
        hourly_key_to_count = defaultdict(lambda: (0, 0))
        hourly_key_to_error_count = defaultdict(lambda: defaultdict(lambda: 0))

        hourly_key_to_med_latency = {}
        for key in hourly_keys:
            lat_list = []
            for element in hour_month_wise_data[vantage_point][key]:
                err, succ = hourly_key_to_count[key]
                if element['error_details'][0]:
                    err += 1
                    hourly_key_to_error_count[key][element['error_details'][1]] += 1
                else:
                    succ += 1
                hourly_key_to_count[key] = (err, succ)
                lat_list.append(element['total_time'])
            import statistics
            hourly_key_to_med_latency[key] = statistics.median(lat_list)

        index = 1
        for key in hourly_keys:
            err, succ = hourly_key_to_count[key]
            if (succ + err) == 0:
                ratio = 1
            else:
                ratio = succ / (succ + err)
            x.append(index)
            y.append(ratio)
            y_lat.append(hourly_key_to_med_latency[key])

            index += 1

        axs[0].plot(x, y, label=vantage_point, c=col_ultra[col_index], linewidth=5, alpha=0.5)
        axs[1].plot(x, y_lat, label=vantage_point, c=col_ultra[col_index])

    axs[0].set_title('Hourly Successful Response Ration for {}'.format(full_desc), fontweight="bold",
                     fontsize='xx-large')
    axs[0].legend(prop={'size': 20})
    plt.sca(axs[0])
    plt.xticks(x_tick_info[0], x_tick_info[1])
    axs[1].set_title('Hourly Median Latency for :{}'.format(full_desc), fontweight="bold", fontsize='xx-large')
    axs[1].legend(prop={'size': 20})
    plt.sca(axs[1])
    plt.suptitle(url_key, fontweight="bold", fontsize='xx-large')
    plt.savefig('{}{}.png'.format(save_dir, "all_graphs.png"), bbox_inches="tight")
    plt.clf()


def get_data(years):
    h_m_acc_to_year = {}
    d_p_cc_to_year = {}

    for year_in_consideration in years:
        for mode in ['nononce_scan']:

            hour_month_wise_data = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: list())))
            data_points_sorted_with_timestamp = defaultdict(lambda: defaultdict(lambda: list()))

            for vantage_point in ["oregon"]:
                print("Done with : {}".format(vantage_point))

                x = root_path + "/" + vantage_point + "/" + mode + "/" + str(year_in_consideration)
                if not os.path.isdir(x):
                    continue
                cld_path = get_directories_or_files_from_path(x, is_dir=False)

                for cld in cld_path:
                    if not is_url_allowed(cld[0], local_env):
                        continue
                    if not local_env:
                        full_path = root_path + "/" + vantage_point + "/" + mode + "/" + str(
                            year_in_consideration) + "/" + cld[0]
                    else:
                        full_path = root_path + "/" + vantage_point + "/" + mode + "/" + str(
                            year_in_consideration) + "/" + cld[0]
                    if not os.path.isfile(full_path):
                        print("Continuing: {}".format(full_path))
                        continue

                    f = open(full_path)
                    json_data = ujson.load(f)

                    for url_key in json_data:
                        print("processing {}".format(url_key))
                        data = json_data[url_key]
                        for element in data:
                            el_copy = element
                            time_str = element['timestamp']
                            error_info = find_error(el_copy)
                            el_copy['error_details'] = error_info
                            date_time_obj = datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S").timetuple()
                            seconds_since_epoch = time.mktime(date_time_obj)
                            month, hour, day = date_time_obj.tm_mon, date_time_obj.tm_hour, date_time_obj.tm_mday
                            # if month == 12:
                            #     continue
                            data_points_sorted_with_timestamp[url_key][vantage_point].append(
                                (int(seconds_since_epoch), el_copy))

                            hour_month_wise_data[url_key][vantage_point][(month, day, hour)].append(el_copy)
                        data_points_sorted_with_timestamp[url_key][vantage_point].sort(key=lambda x: x[0])
            #as_2_ip = AS2ISP()
            # for key in data_points_sorted_with_timestamp:
            #     plot_graphs_v2(hour_month_wise_data[key], year_in_consideration, mode, key, as_2_ip)

        h_m_acc_to_year[year_in_consideration] = hour_month_wise_data
        d_p_cc_to_year[year_in_consideration] = data_points_sorted_with_timestamp

    return h_m_acc_to_year, d_p_cc_to_year