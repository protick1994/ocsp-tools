from collections import defaultdict
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import ujson
import os
from config import *
import time
import datetime
from helper import *


local_env = False
if local_env:
    root_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/ocsp_url_wise_dumps"
    result_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/graphs"
else:
    root_path = "/net/data/cert-pinning/temp-protick/v9/url_wise"
    result_path = "/net/data/cert-pinning/temp-protick/v9/graphs"


error_set = set()
from collections import defaultdict
d = defaultdict(lambda : 0)


def plot_graphs(data_points_sorted_with_timestamp, hour_month_wise_data, url_key, year_in_consideration, mode, vantage_point):
    plt.rcParams["font.weight"] = "bold"
    plt.rcParams["axes.labelweight"] = "bold"

    full_desc = url_key + "-{}-{}-{}".format(year_in_consideration, mode, vantage_point)
    save_dir = '{}/{}/{}/{}/{}/'.format(result_path, vantage_point, mode, year_in_consideration, proc_url(url_key))

    Path(save_dir).mkdir(parents=True, exist_ok=True)

    fig, axs = plt.subplots(5, 1, figsize=(30, 30))

    x_arr = []
    y_arr = []
    mod_arr = []
    pre = -10
    for element in data_points_sorted_with_timestamp:
        seconds_since_epoch = int(element[0])
        latency, has_error = element[1]['total_time'], element[1]['error_details'][0]

        if seconds_since_epoch == pre:
            if has_error is True:
                mod_arr[-1] = (seconds_since_epoch, (latency, has_error))
        else:
            mod_arr.append((seconds_since_epoch, (latency, has_error)))

        pre = seconds_since_epoch

    master_arr = mod_arr
    master_arr.sort()

    for element in master_arr:
        x_arr.append(int(element[0]))
        y_arr.append(element[1])

    x2 = np.array(x_arr)
    k = np.array([e[0] for e in y_arr])

    # find out which parameters are more than 1.5*std away from mean
    is_error = np.array([e[1] for e in y_arr])

    axs[0].set_title('Error Distribution for {}'.format(full_desc), fontweight="bold", fontsize='xx-large')
    axs[0].scatter(x2[~is_error], k[~is_error], label='successful', c='b')
    axs[0].scatter(x2[is_error], k[is_error], label='error', c='r')
    axs[0].legend(prop={'size': 20})

    hourly_keys = list(hour_month_wise_data.keys())
    hourly_keys.sort()

    x_tick_info = [[], []]

    x = []
    y = []
    y_lat = []
    hourly_key_to_count = defaultdict(lambda: (0, 0))
    hourly_key_to_error_count = defaultdict(lambda: defaultdict(lambda: 0))
    hourly_key_to_neterror_count = defaultdict(lambda: defaultdict(lambda: 0))
    error_types = ['net_error', 'non_200_http_code', 'undefined', 'malformed_response']

    diff_net_errors = set()
    for key in net_error:
        diff_net_errors.add(net_error[key])
    diff_net_errors = list(diff_net_errors)


    hourly_key_to_med_latency = {}
    for key in hourly_keys:
        lat_list = []
        for element in hour_month_wise_data[key]:
            err, succ = hourly_key_to_count[key]
            if element['error_details'][0]:
                err += 1
                hourly_key_to_error_count[key][element['error_details'][1]] += 1
                if element['error_details'][1] == 'net_error':
                    hourly_key_to_neterror_count[key][element['error_details'][2]] += 1
            else:
                succ += 1
            hourly_key_to_count[key] = (err, succ)
            lat_list.append(element['total_time'])
        import statistics
        hourly_key_to_med_latency[key] = statistics.median(lat_list)

    index = 1
    month = 1

    error_x = [[] for i in range(len(error_types))]
    net_error_x = [[] for i in range(len(diff_net_errors))]

    for key in hourly_keys:


        err, succ = hourly_key_to_count[key]
        if (succ + err) == 0:
            ratio = 1
        else:
            ratio = succ / (succ + err)

        x.append(index)
        if key[0] == month:
            x_tick_info[0].append(index)
            import calendar
            x_tick_info[1].append(calendar.month_name[month])
            month += 1


        y.append(ratio)
        y_lat.append(hourly_key_to_med_latency[key])

        err_index = 0
        for error_key in error_types:
            error_x[err_index].append(hourly_key_to_error_count[key][error_key])
            err_index += 1

        err_index = 0
        for net_error_key in diff_net_errors:
            net_error_x[err_index].append(hourly_key_to_neterror_count[key][net_error_key])
            err_index += 1


        index += 1

    axs[1].set_title('Hourly Successful Response Ration for {}'.format(full_desc), fontweight="bold", fontsize='xx-large')
    axs[1].plot(x, y, label='hourly successful response ratio', c='b')
    axs[1].legend(prop={'size': 20})
    plt.sca(axs[1])
    plt.xticks(x_tick_info[0], x_tick_info[1])

    axs[2].set_title('Hourly Median Latency for :{}'.format(full_desc), fontweight="bold", fontsize='xx-large')
    axs[2].plot(x, y_lat, label='median latency', c='r')
    axs[2].legend(prop={'size': 20})
    plt.sca(axs[2])
    plt.xticks(x_tick_info[0], x_tick_info[1])

    axs[3].set_title('Hourly Error Distribution for :{}'.format(full_desc), fontweight="bold", fontsize='xx-large')

    err_index = 0
    for error_key in error_types:
        #plt.plot(x, error_x[err_index], label=error_key, c=col_ultra[err_index])
        axs[3].plot(x, error_x[err_index], label=error_key, c=col_ultra[err_index])
        #error_x[err_index].append(hourly_key_to_error_count[error_key][error_key])
        err_index += 1
    axs[3].legend(prop={'size': 15})
    plt.sca(axs[3])
    plt.xticks(x_tick_info[0], x_tick_info[1])

    axs[4].set_title('Hourly Netowrk Error Distribution for :{}'.format(full_desc), fontweight="bold", fontsize='xx-large')

    err_index = 0
    for error_key in diff_net_errors:
        axs[4].plot(x, net_error_x[err_index], label=error_key, c=col_ultra[err_index])
        err_index += 1

    axs[4].legend(prop={'size': 15})
    plt.sca(axs[4])
    plt.xticks(x_tick_info[0], x_tick_info[1])

    plt.suptitle(url_key, fontweight="bold", fontsize='xx-large')

    plt.savefig('{}{}.png'.format(save_dir, "all_graphs.png"), bbox_inches="tight")
    plt.clf()



def plot_graphs_v2(data_points_sorted_with_timestamp, hour_month_wise_data, year_in_consideration, mode, vantage_point):
    plt.rcParams["font.weight"] = "bold"
    plt.rcParams["axes.labelweight"] = "bold"

    full_desc = "All Responders" + "-{}-{}-{}".format(year_in_consideration, mode, vantage_point)
    save_dir = '{}/{}/{}/{}/'.format(result_path, vantage_point, mode, year_in_consideration)

    Path(save_dir).mkdir(parents=True, exist_ok=True)

    fig, axs = plt.subplots(5, 1, figsize=(30, 30))

    x_arr = []
    y_arr = []
    mod_arr = []
    pre = -10
    for element in data_points_sorted_with_timestamp:
        seconds_since_epoch = int(element[0])
        latency, has_error = element[1]['total_time'], element[1]['error_details'][0]

        if seconds_since_epoch == pre:
            if has_error is True:
                mod_arr[-1] = (seconds_since_epoch, (latency, has_error))
        else:
            mod_arr.append((seconds_since_epoch, (latency, has_error)))

        pre = seconds_since_epoch

    master_arr = mod_arr
    master_arr.sort()

    for element in master_arr:
        x_arr.append(int(element[0]))
        y_arr.append(element[1])

    x2 = np.array(x_arr)
    k = np.array([e[0] for e in y_arr])

    # find out which parameters are more than 1.5*std away from mean
    is_error = np.array([e[1] for e in y_arr])

    axs[0].set_title('Error Distribution for {}'.format(full_desc), fontweight="bold", fontsize='xx-large')
    axs[0].scatter(x2[~is_error], k[~is_error], label='successful', c='b')
    axs[0].scatter(x2[is_error], k[is_error], label='error', c='r')
    axs[0].legend(prop={'size': 20})

    hourly_keys = list(hour_month_wise_data.keys())
    hourly_keys.sort()

    x_tick_info = [[], []]

    x = []
    y = []
    y_lat = []
    hourly_key_to_count = defaultdict(lambda: (0, 0))
    hourly_key_to_error_count = defaultdict(lambda: defaultdict(lambda: 0))
    hourly_key_to_neterror_count = defaultdict(lambda: defaultdict(lambda: 0))
    error_types = ['net_error', 'non_200_http_code', 'undefined', 'malformed_response']

    diff_net_errors = set()
    for key in net_error:
        diff_net_errors.add(net_error[key])
    diff_net_errors = list(diff_net_errors)


    hourly_key_to_med_latency = {}
    for key in hourly_keys:
        lat_list = []
        for element in hour_month_wise_data[key]:
            err, succ = hourly_key_to_count[key]
            if element['error_details'][0]:
                err += 1
                hourly_key_to_error_count[key][element['error_details'][1]] += 1
                if element['error_details'][1] == 'net_error':
                    hourly_key_to_neterror_count[key][element['error_details'][2]] += 1
            else:
                succ += 1
            hourly_key_to_count[key] = (err, succ)
            lat_list.append(element['total_time'])
        import statistics
        hourly_key_to_med_latency[key] = statistics.median(lat_list)

    index = 1
    month = 1

    error_x = [[] for i in range(len(error_types))]
    net_error_x = [[] for i in range(len(diff_net_errors))]

    for key in hourly_keys:


        err, succ = hourly_key_to_count[key]
        if (succ + err) == 0:
            ratio = 1
        else:
            ratio = succ / (succ + err)

        x.append(index)
        if key[0] == month:
            x_tick_info[0].append(index)
            import calendar
            x_tick_info[1].append(calendar.month_name[month])
            month += 1


        y.append(ratio)
        y_lat.append(hourly_key_to_med_latency[key])

        err_index = 0
        for error_key in error_types:
            error_x[err_index].append(hourly_key_to_error_count[key][error_key])
            err_index += 1

        err_index = 0
        for net_error_key in diff_net_errors:
            net_error_x[err_index].append(hourly_key_to_neterror_count[key][net_error_key])
            err_index += 1


        index += 1

    axs[1].set_title('Hourly Successful Response Ration for {}'.format(full_desc), fontweight="bold", fontsize='xx-large')
    axs[1].plot(x, y, label='hourly successful response ratio', c='b')
    axs[1].legend(prop={'size': 20})
    plt.sca(axs[1])
    plt.xticks(x_tick_info[0], x_tick_info[1])

    axs[2].set_title('Hourly Median Latency for :{}'.format(full_desc), fontweight="bold", fontsize='xx-large')
    axs[2].plot(x, y_lat, label='median latency', c='r')
    axs[2].legend(prop={'size': 20})
    plt.sca(axs[2])
    plt.xticks(x_tick_info[0], x_tick_info[1])



    axs[3].set_title('Hourly Error Distribution for :{}'.format(full_desc), fontweight="bold", fontsize='xx-large')

    err_index = 0
    for error_key in error_types:
        #plt.plot(x, error_x[err_index], label=error_key, c=col_ultra[err_index])
        axs[3].plot(x, error_x[err_index], label=error_key, c=col_ultra[err_index])
        #error_x[err_index].append(hourly_key_to_error_count[error_key][error_key])
        err_index += 1
    axs[3].legend(prop={'size': 15})
    plt.sca(axs[3])
    plt.xticks(x_tick_info[0], x_tick_info[1])

    axs[4].set_title('Hourly Netowrk Error Distribution for :{}'.format(full_desc), fontweight="bold", fontsize='xx-large')

    err_index = 0
    for error_key in diff_net_errors:
        axs[4].plot(x, net_error_x[err_index], label=error_key, c=col_ultra[err_index])
        err_index += 1

    axs[4].legend(prop={'size': 15})
    plt.sca(axs[4])
    plt.xticks(x_tick_info[0], x_tick_info[1])

    plt.suptitle("All Responders", fontweight="bold", fontsize='xx-large')

    plt.savefig('{}{}.png'.format(save_dir, "all_graphs.png"), bbox_inches="tight")
    plt.clf()




## TODO check again with all files
# f = open("all_error_reasons_with_count.json")
# json_data = ujson.load(f)
# for e in json_data:
#     matched = False
#     for key in net_error:
#         if key in e[0]:
#             matched = True
#             break
#     if not matched:
#         print(e[0])
#
# a = 1

all_info = []

from collections import defaultdict

hour_month_wise_data_master = defaultdict(lambda: list())
data_points_sorted_with_timestamp_master = []

for vantage_point in ['oregon']:
    print("Starting vantage point: {}".format(vantage_point))
    for mode in ['nononce_scan']:
        print("Starting mode: {}".format(mode))

        # Here starts year
        for year_in_consideration in [2021]:
            full_path = root_path + "/" + vantage_point + "/" + mode + "/" + str(year_in_consideration) + "/all_urls.json"
            # TODO fix
            if not os.path.isfile(full_path):
                print("Continuing: {}".format(full_path))
                continue
            # TODO Change here
            if local_env:
                full_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/serpro.json"
            f = open(full_path)
            json_data = ujson.load(f)

            for url_key in json_data:
                data = json_data[url_key]
                hour_month_wise_data = defaultdict(lambda : list())
                data_points_sorted_with_timestamp = []
                for element in data:
                    el_copy = element
                    time_str = element['timestamp']
                    error_info = find_error(el_copy)
                    el_copy['error_details'] = error_info
                    date_time_obj = datetime.datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S").timetuple()
                    seconds_since_epoch = time.mktime(date_time_obj)
                    month, hour, day = date_time_obj.tm_mon, date_time_obj.tm_hour, date_time_obj.tm_mday
                    if month == 12:
                        continue
                    data_points_sorted_with_timestamp.append((int(seconds_since_epoch), el_copy))
                    data_points_sorted_with_timestamp_master.append((int(seconds_since_epoch), el_copy))

                    hour_month_wise_data[(month, day, hour)].append(el_copy)
                    hour_month_wise_data_master[(month, day, hour)].append(el_copy)
                data_points_sorted_with_timestamp.sort(key=lambda x: x[0])
                data_points_sorted_with_timestamp_master.sort(key=lambda x: x[0])
                #plot_graphs(data_points_sorted_with_timestamp, hour_month_wise_data, url_key, year_in_consideration, mode, vantage_point)

            plot_graphs_v2(data_points_sorted_with_timestamp_master, hour_month_wise_data_master, year_in_consideration, mode, vantage_point)


