import json

f = open("amulgum.json")

d = json.load(f)
a = 1

def get_tup(d):
    tot = 0
    other = 0
    for key in d:
        if key == 'non_200':
            tot = d[key]
        else:
            other += d[key]
    if tot - other < 0:
        a = 1
    return tot, other, tot - other

from collections import defaultdict

vantage_to_short_all = defaultdict(lambda : 0)
vantage_to_short_http = defaultdict(lambda : 0)
vantage_to_short_other = defaultdict(lambda : 0)

vantage_to_long_all = defaultdict(lambda : 0)
vantage_to_long_http = defaultdict(lambda : 0)
vantage_to_long_other = defaultdict(lambda : 0)

vantage_set = set()

hit_list = [
    'ocsp.comodoca.com',
    'ocsp.sectigo.com',
    'ocsp.pki.goog',
    'r3.o.lencr.org',
    'ocsp.ocsp.com',
    'ocsp.godaddy.com',
    'ocsp2.globalsign.com',
    'ocsp.globalsign.com',
    'ocsp-certum.com',
    'dv.g4.ocsp.pucert.jprs.jp'
]


def hit_url(url):
    for e in hit_list:
        if e in url:
            return True
    return False

url_set = set()

for element in d:
    vantage = element[0][0]
    vantage_set.add(vantage)

    url = element[0][1]

    # if 'goog' not in url_set:
    url_set.add(url)


    short_tuple = get_tup(element[1])
    long_tup = get_tup(element[2])

    vantage_to_short_all[vantage] += short_tuple[0]
    vantage_to_short_http[vantage] += short_tuple[1]
    vantage_to_short_other[vantage] += short_tuple[2]

    vantage_to_long_all[vantage] += long_tup[0]
    vantage_to_long_http[vantage] += long_tup[1]
    vantage_to_long_other[vantage] += long_tup[2]

a = 1

print("Vantage", "Short-all", "Short http", "Short-other")
for vantage in vantage_set:
    print(vantage, vantage_to_short_all[vantage], vantage_to_short_http[vantage], vantage_to_short_other[vantage])

print()
print()
print("Vantage", "Long-all", "Long http", "Long-other")
for vantage in vantage_set:
    print(vantage, vantage_to_long_all[vantage], vantage_to_long_http[vantage], vantage_to_long_other[vantage])

# Vantage Short-all Short http Short-other
# sao-paulo 6363 6523 -160
# sydney 3937 4116 -179
# paris 5972 6517 -545
# seoul 2045 2184 -139
# oregon 4415 4570 -155
# virginia 6231 7143 -912
#
#
# Vantage Long-all Long http Long-other
# sao-paulo 61 111 -50
# sydney 50 71 -21
# paris 62 191 -129
# seoul 44 52 -8
# oregon 54 107 -53
# virginia 81 233 -152



# Vantage Short-all Short http Short-other
# paris 165 173 -8
# virginia 262 276 -14
# seoul 39 39 0
# sydney 36 36 0
# oregon 203 211 -8
# sao-paulo 152 155 -3
#
#
# Vantage Long-all Long http Long-other
# paris 6 5 1
# virginia 5 4 1
# seoul 3 3 0
# sydney 2 2 0
# oregon 6 5 1
# sao-paulo 5 5 0


