import os
from collections import defaultdict
from pathlib import Path

import ujson

from config import *
from helper import *

# oregon  paris  sao-paulo  seoul  sydney  virginia

'''
UnableToParse	StatusCode_415	ServerDown	NameLookupFail	
StatusCode_530	StatusCode_302	NetworkError	StatusCode_408	
OCSPCertsInvalid	StatusCode_503	StatusCode_502	NoError	StatusCode_500	
StatusCode_504	StatusCode_403	StatusCode_400	StatusCode_0	StatusCode_524	
StatusCode_404	StatusCode_405
'''
# v6 e without parsing data ache
local_env = False
if local_env:
    root_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/data_dump"
    result_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/ocsp_parse_results/exp3"
else:
    root_path = "/net/data-backedup/ocsp/ocsp-scan/"
    result_path = "/net/data/cert-pinning/temp-protick/v_14/init_dump"


def parseOCSPResponse(j):
    r = {}
    r['ocspURL'] = j['ocsp_url']
    r['total_time'] = j['response']['total_time']
    r['namelookup_time'] = j['response']['namelookup_time']
    r['err_reason'] = j['response']['err_reason']
    r['response_code'] = j['response']['response_code']
    r['http_code'] = j['response']['http_code']
    r['err_code'] = j['response']['err_code']
    r["iteration"] = j["iteration"]
    r['timestamp'] = j['timestamp']

    if 'primary_ip' in j['response']:
        r['primary_ip'] = j['response']['primary_ip']

    parse_info = parse_actual_response(j)
    r["parsing_info"] = parse_info
    return r


def get_directories_or_files_from_path(path, is_dir = True):
    retset = set()
    #print(path)
    for parent_path in os.listdir(path):
        #print(parent_path)

        if not os.path.isdir(path + "/" + parent_path):
            if is_dir:
                continue
        else:
            if not is_dir:
                continue
        retset.add((parent_path, path + "/" + parent_path))

    return list(retset)


url_val_count = defaultdict(lambda: 0)

for vantage_point in vantage_points:
    print("Starting vantage point: {}".format(vantage_point))
    for mode in modes:
        print("Starting mode: {}".format(mode))
        full_path = root_path + "/" + vantage_point + "/" + mode
        if not os.path.isdir(full_path):
            continue
        for year_in_consideration in years:
            print("Starting year: {}".format(year_in_consideration))

            # print("Path : {}".format(full_path))
            directories = get_directories_or_files_from_path(full_path)
            # print("Got total dirs {}".format(len(directories)))

            d = defaultdict(lambda: list())

            for dir in directories:
                year, month, day = dir[0].split("-")
                year, month, day = int(year), int(month), int(day)
                full_dir = dir[1]
                if year != year_in_consideration:
                    continue
                d[month].append(dir)

            for key in d:
                d[key].sort()
            #print(list(d.keys()))
            for month in range(13):
                error_set = set()
                if month not in d:
                    #print("Skipping month {}".format(month))
                    continue
                month_to_ocsp_info_dump = defaultdict(lambda: list())
                #print("Keys in month {}: d[month] {}".format(month, len(d[month])))
                for element in d[month]:
                    try:
                        child_dir, full_path_nested = element
                        time_wise_sub_dirs = get_directories_or_files_from_path(full_path_nested)
                        time_wise_sub_dirs.sort()
                        for sub_dir in time_wise_sub_dirs:
                            try:
                                #file_to_read = sub_dir[1] + "/" + "scans.txt"
                                if os.path.isfile(sub_dir[1] + "/" + "scans.txt"):
                                    file_to_read = sub_dir[1] + "/" + "scans.txt"
                                    file_buffer = open(file_to_read)
                                elif os.path.isfile(sub_dir[1] + "/" + "scans.txt.xz"):
                                    file_to_read = sub_dir[1] + "/" + "scans.txt.xz"
                                    import lzma
                                    file_buffer = lzma.open(file_to_read, mode='rt')
                                else:
                                    continue

                                # TODO error txt te ki ase
                                for line in file_buffer:
                                    try:
                                        s = ujson.loads(line.rstrip())
                                    except Exception as e:
                                        #print("Error in reading json {}: {}".format(file_to_read, e))
                                        continue
                                    try:
                                        r = parseOCSPResponse(s)
                                        error_set.add(r['err_reason'])
                                        ocspURL = r['ocspURL']
                                        url_val_count[ocspURL] += 1
                                        r.pop('ocspURL', None)
                                        month_to_ocsp_info_dump[ocspURL].append(r)

                                    except Exception as e:
                                        #print("Error in parsing json {}: {}".format(file_to_read, e))
                                        pass
                            except Exception as e:
                                pass

                    except Exception as e:
                        pass
                path_to_carry = result_path + "/{}/{}/{}/{}".format(vantage_point, mode, year_in_consideration, month)

                Path(path_to_carry).mkdir(parents=True, exist_ok=True)

                #print("Dumping these keys {}".format(list(month_to_ocsp_info_dump.keys())))

                with open("{}/result.json".format(path_to_carry), "w") as ouf:
                    ujson.dump(month_to_ocsp_info_dump, ouf)

                with open("{}/error.json".format(path_to_carry), "w") as ouf:
                    ujson.dump(list(error_set), ouf)

                month_to_ocsp_info_dump.clear()
                error_set.clear()

                #print("Finished with month {}".format(month))

            d.clear()
            #print("Finished with year {}".format(year_in_consideration))
    #print("Finished with vantage point {}".format(vantage_point))

url_val_count_list = []
for key in url_val_count:
    url_val_count_list.append((key, url_val_count[key]))

url_val_count_list.sort(key=lambda x: x[1], reverse=True)

path_to_dump_error_list = result_path
with open(path_to_dump_error_list + "/url_data_count.json", "w") as ouf:
    ujson.dump(url_val_count_list, ouf)


