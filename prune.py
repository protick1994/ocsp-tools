import json

f = open("urls_to_cdn.json")

d = json.load(f)

a = 1
ans = []
for e in d:
    if 'Akamai' in e[2] or 'akamai' in e[2]:
        ans.append(e[0])

with open("akami_urls.json", "w") as ouf:
    json.dump(ans, ouf, indent=2)
