from collections import defaultdict
from pathlib import Path
import os
import ujson
from config import *

local_env = False
if local_env:
    root_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/ocsp_parse_results/exp3"
    result_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/ocsp_url_wise_dumps_v2"
else:
    root_path = "/net/data/cert-pinning/temp-protick/v_14/init_dump"
    result_path = "/net/data/cert-pinning/temp-protick/v_14/url_wise"

# vantage_point = "virginia"
# mode = "nononce_scan"
# year_in_consideration = 2021


for vantage_point in vantage_points:
    print("Starting vantage point: {}".format(vantage_point))
    for mode in modes:
        print("Starting mode: {}".format(mode))
        full_path = root_path + "/" + vantage_point + "/" + mode
        if not os.path.isdir(full_path):
            print("Continuing: {}".format(full_path))
            continue
        for year_in_consideration in years:
            print("Starting year: {}".format(year_in_consideration))

            full_path = root_path + "/" + vantage_point + "/" + mode + "/" + str(year_in_consideration)

            d = defaultdict(lambda: list())

            for month in range(1, 13):
                try:
                    actual_path = full_path + "/{}".format(month) + "/result.json"
                    if not os.path.isdir(full_path + "/{}".format(month)):
                        continue
                    f = open(actual_path)
                    json_data = ujson.load(f)
                    for key in json_data:
                        try:
                            d[key].extend(json_data[key])
                        except Exception:
                            pass
                except Exception:
                    pass

            for key in d:
                url = key
                if url.startswith("https://"):
                    url = url[8:]
                elif url.startswith("http://"):
                    url = url[7:]

                url = url.replace("/", "\\")

                path_to_dump_url_wise_data = result_path + "/{}/{}/{}/".format(vantage_point, mode, year_in_consideration)
                Path(path_to_dump_url_wise_data).mkdir(parents=True, exist_ok=True)
                with open(path_to_dump_url_wise_data + "/{}.json".format(url), "w") as ouf:
                    ujson.dump({key: d[key]}, ouf)


            path_to_dump_all_url_wise_data = result_path + "/{}/{}/{}/".format(vantage_point, mode, year_in_consideration)
            Path(path_to_dump_all_url_wise_data).mkdir(parents=True, exist_ok=True)
            with open(path_to_dump_all_url_wise_data + "/all_urls.json", "w") as ouf:
                ujson.dump(d, ouf)

            d.clear()