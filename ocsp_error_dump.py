from collections import defaultdict
from pathlib import Path
from config import *
import ujson
import os

local_env = True
if local_env:
    root_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/ocsp_parse_results/exp3"
    result_path = "/Users/protick.bhowmick/PriyoRepos/ocsp-tools/error_set"
else:
    root_path = "/net/data/cert-pinning/temp-protick/v8/init_dump"
    result_path = "/net/data/cert-pinning/temp-protick/v8/error_set"


error_set = set()
from collections import defaultdict
d = defaultdict(lambda: 0)

for vantage_point in vantage_points:
    for mode in modes:
        for year in years:
            for month in range(1, 13):
                try:
                    actual_path = root_path + "/" + vantage_point + "/" + mode + "/" + str(year) + "/{}".format(month) + "/error.json"
                    if not os.path.isfile(actual_path):
                        continue
                    f = open(actual_path)
                    json_data = ujson.load(f)
                    error_set.update(json_data)
                    for element in json_data:
                        d[element] += 1
                except Exception:
                    pass

            path_to_dump_all_url_wise_data = result_path
            Path(path_to_dump_all_url_wise_data).mkdir(parents=True, exist_ok=True)
            with open(path_to_dump_all_url_wise_data + "/all_error_reasons.json", "w") as ouf:
                ujson.dump(list(error_set), ouf, indent=2)

            error_reason_with_count = []
            for key in d:
                error_reason_with_count.append((key, d[key]))
            error_reason_with_count.sort(key=lambda x: x[1], reverse=True)

            with open(path_to_dump_all_url_wise_data + "/all_error_reasons_with_count.json", "w") as ouf:
                ujson.dump(error_reason_with_count, ouf, indent=2)
