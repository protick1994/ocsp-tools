import base64

from cryptography.x509 import ocsp
from pyasn1.codec.der.decoder import decode as der_decoder
from pyasn1.codec.native.encoder import encode as native_encoder
from pyasn1_modules import rfc2560


col_ultra = ['darkolivegreen', 'darkgoldenrod', 'crimson', 'dodgerblue', 'blueviolet',
                'royalblue', 'darkcyan', 'darkorange', 'purple', 'navy',
                'gold', 'indigo', 'tomato', 'sienna', 'steelblue', ]

def readMapAKItoCert(path):
    m = {}
    for line in open(path):
        authorityKeyIdentifier, cert = line.rstrip().split("\t")
        m[authorityKeyIdentifier] = cert
    return m

def return_ocsp_result(ocsp_response, is_bytes=True):
    """ Extract the OCSP result from the provided ocsp_response """
    try:
        if is_bytes:
            ocsp_response = ocsp.load_der_ocsp_response(ocsp_response)
        else:
            ocsp_response = ocsp.load_der_ocsp_response(ocsp_response.content)

        return ocsp_response

    except ValueError as err:
        # print(f"{str(err)}")
        return f"{str(err)}"
# 'MAMKAQY='

def proc_url(url):
    if url.startswith("https://"):
        url = url[8:]
    elif url.startswith("http://"):
        url = url[7:]

    url = url.replace("/", "\\")
    return url

def parse_actual_response(j):
    try:
        ob = j['response']['response_body']
        der = base64.b64decode(ob)
        ocsp_result = return_ocsp_result(der)
        if isinstance(ocsp_result, str):
            return (False, ocsp_result)
        else:
            return (True, "")
    except Exception as e:
        return (False, "Unknown_parsing_error")

net_error = {

    "Recv failure": "data_recv_failure",
    "not resolve host": "host_resolution",
    "Resolving timed out": "host_resolution",
    "timed out": "time_out",
    "time-out": "time_out",
    "Failed to connect": "connection_failure",
    "Empty reply": "empty_reply",
    "unable to get local issuer certificate": "unable_to_get_issuer_certificate"
  }


def find_error(element):
    if element['http_code'] == 200:
        if element['parsing_info'][0]:
            return (False, 'Successful', '')
        else:
            return (True, 'Malformed Response', '')
    else:
        if element['err_reason'] != '':
            for key in net_error:
                if key in element['err_reason']:
                    return (True, 'net_error', net_error[key])
            #TODO think again about label
            return (True, 'net_error', 'unknown')
        elif element['http_code'] != 200:
            return (True, 'non_200_http_code', "{}_http_code".format(element['http_code']))
        else:
            return (True, 'undefined', '')



def is_hosted_by_cdn(org):
    CDN_HINTS = ["Akam", "cloud", "edge", "fast", "tiny", 'CDN', 'Cloudfront']

    CDNS = ['Akamai',
            'Amazon',
            'Bitgravity',
            'Cachefly',
            'CDN77',
            'CDNetworks',
            'CDNify',
            'ChinaCache',
            'ChinaNetCenter',
            'EdgeCast',
            'Fastly',
            'Highwinds',
            'Internap',
            'KeyCDN',
            'Level3',
            'Limelight',
            'MaxCDN',
            'NetDNA',
            'Telefónica',
            'XCDN',
            'CloudFlare',
            'Jetpack',
            'Rackspac',
            'CloudLayer',
            'CloudCache',
            'TinyCDN',
            'Amazon',
            'Incapsula',
            'jsDelivr',
            'EdgeCast',
            'CDNsun',
            'Limelight',
            'Azure',
            'CDNlio',
            'SoftLayer',
            'ITWorks',
            'CloudOY',
            'Octoshape',
            'Hibernia',
            'WebMobi',
            'CDNvideo']

    org_lower = org.lower()
    for key in CDNS + CDN_HINTS:
        lower_key = key.lower()
        if lower_key in org_lower:
            return True
